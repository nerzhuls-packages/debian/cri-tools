#! /bin/bash

set -e
set -u

PKG_ARCH=${1}

for PKG in cri-tools; do
    echo "Packaging ${PKG}"
    mkdir -p ${PKG}/usr/bin/
    cd ${PKG}/usr/bin/
    tar xvzf ../../../build/crictl-v${CRI_TOOLS_VERSION}-linux-${PKG_ARCH}.tar.gz
    cd -
    # Replace versions
    sed -i 's/%%CRI_TOOLS_VERSION%%/'${CRI_TOOLS_PACKAGE_VERSION}'/g' ${PKG}/DEBIAN/control
    sed -i 's/amd64/'${PKG_ARCH}'/g' ${PKG}/DEBIAN/control
    dpkg-deb -b ${PKG}
    mv ${PKG}.deb dist/${PKG}-${PKG_ARCH}.deb
done
