#! /bin/bash

for BN in cri-tools; do
    find ${BN} -type d -exec chmod 755 {} \;
    find ${BN} -type f -not -name postinst -exec chmod 644 {} \;
    find ${BN} -type f -name postinst -exec chmod 755 {} \;
done

mkdir -p build/ dist/
apt-get -qy update > /dev/null
apt-get -qyy install wget > /dev/null
